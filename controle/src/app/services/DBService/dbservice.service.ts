import { Injectable } from '@angular/core';
import { InterfaceDAO } from 'src/app/Interfaces/IntefaceDAO';
import { Usuario } from 'src/app/models/Usuario';


@Injectable({
  providedIn: 'root'
})
export class DBServiceService implements InterfaceDAO<Object>{

  constructor() { 

  }
  //Implementação de getByID para Usuario
  getByID(ID : String, clazz : Usuario) : Usuario{
    return  JSON.parse(localStorage.getItem(clazz.enrollID));
  }

  setData(clazz : Usuario) : boolean{
    localStorage.setItem(clazz.enrollID,JSON.stringify(clazz));
    return true;
  }

}
