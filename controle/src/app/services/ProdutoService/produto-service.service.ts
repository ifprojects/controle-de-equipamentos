import { Injectable } from '@angular/core';
import { Equipment } from 'src/app/models/Equipamento';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Injectable({
  providedIn: 'root'
})
export class ProdutoServiceService {


  
  constructor() { 
    
  }


  addProduct(equipment : Equipment){
    localStorage.setItem("product" + equipment.getId(),JSON.stringify(equipment));
  }
  
  listProduct() : Array<Equipment>{
    var productArray : Array<Equipment>;
    productArray = new Array();
    var i : any;
    for(i in localStorage){
      if(i.match('product') && i != undefined){
        productArray.push(JSON.parse(localStorage.getItem(i)));
      }
    }
    return productArray;
  }
  
  removeProduct(id : string){
    localStorage.removeItem("product"+id);
  }

  updateProduct(equipment : Equipment) {
    localStorage.setItem("product"+ equipment.getId(), JSON.stringify(equipment));
  }
}
