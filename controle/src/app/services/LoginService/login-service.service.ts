import { Injectable } from '@angular/core';
import { DBServiceService } from '../DBService/dbservice.service';
import { Usuario } from 'src/app/models/Usuario';


@Injectable({
  providedIn: 'root',
})
export class LoginServiceService {

  private db_service : DBServiceService;

  constructor() { 
    this.db_service = new DBServiceService();
  }

  public login(usuario : Usuario) : boolean{
    let usuarioAux = this.db_service.getByID(usuario.enrollID, usuario);

    if(usuario['_password'] === usuarioAux['_password']){
      return true;
    }
    return false;
  }

  public logout(){
    
  }

  public signup(usuario : Usuario){

    if(this.db_service.setData(usuario)){
    }
  }
  
}
