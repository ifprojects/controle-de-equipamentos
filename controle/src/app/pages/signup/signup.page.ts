import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from 'src/app/services/LoginService/login-service.service';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  private signup_service : LoginServiceService;
  private usuario: Usuario;

  // atributos
  name : string;
  enrollID : string;
  password : string;
  signStatus : boolean = true;

  constructor(public route : Router) { 
    this.usuario = new Usuario();
    this.signup_service = new LoginServiceService();
  }

  signup(){
    if(this.name == null || this.enrollID == null || this.password == null){
      return this.signStatus = false;
    }
    this.usuario.name = this.name
    this.usuario.enrollID = this.enrollID
    this.usuario.password = this.password;
    this.signup_service.signup(this.usuario);
    this.signStatus = true;
    this.route.navigateByUrl('/login');
  }

  ngOnInit() {
  }

}
