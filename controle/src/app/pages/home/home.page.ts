import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from 'src/app/services/LoginService/login-service.service';
import { Routes, RoutesRecognized, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddEquipamentoPage } from '../modals/add-equipamento/add-equipamento.page';
import { ProdutoServiceService } from 'src/app/services/ProdutoService/produto-service.service';
import { Equipment } from 'src/app/models/Equipamento';
import { ChangeEquipamentoPage } from '../modals/change-equipamento/change-equipamento.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private login_service: LoginServiceService;
  private productService: ProdutoServiceService;

  public arrayOfProducts: Array<Equipment>;

  constructor(public routes: Router, public modal: ModalController) {
    this.login_service = new LoginServiceService();
    this.productService = new ProdutoServiceService();
    this.arrayOfProducts = new Array<Equipment>();
  }


  ngOnInit(): void {
    this.listProduct();
  }

  logout() {
    this.login_service.logout();
    this.routes.navigateByUrl('/login');
  }

  listProduct() {
    return this.arrayOfProducts = this.productService.listProduct();
  }

  deleteProduct(id: string) {
    this.productService.removeProduct(id);
  }

  refresh(){
    this.ngOnInit();
  }



  async addEquipamento() {
    let equipamentoModal = await this.modal.create({
      component: AddEquipamentoPage,
    });
    return await equipamentoModal.present();

  }

  async changeEquipamento(itemId,itemName){
    let equipamentoModal = await this.modal.create({
      component : ChangeEquipamentoPage,
      componentProps : {
        itemId : itemId,
        itemName : itemName,
      }
        
    });
    return await equipamentoModal.present();
  }

}
