import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEquipamentoPage } from './add-equipamento.page';

describe('AddEquipamentoPage', () => {
  let component: AddEquipamentoPage;
  let fixture: ComponentFixture<AddEquipamentoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEquipamentoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEquipamentoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
