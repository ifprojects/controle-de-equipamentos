import { Component, OnInit } from '@angular/core';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { Equipment } from 'src/app/models/Equipamento';
import { ProdutoServiceService } from 'src/app/services/ProdutoService/produto-service.service';


import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';


@Component({
  selector: 'app-add-equipamento',
  templateUrl: './add-equipamento.page.html',
  styleUrls: ['./add-equipamento.page.scss'],
})
export class AddEquipamentoPage implements OnInit {

  public name: string;
  public id: string;
  public data: Date;
  imageURI: string;

  private productSevice: ProdutoServiceService;


  constructor(public modal: ModalController, private camera: Camera,
    public actionSheetController: ActionSheetController,
    private file: File) {
    this.productSevice = new ProdutoServiceService();
  }

  ngOnInit() {


  }

  addProduct() {
    this.productSevice.addProduct(this.packEquipamento());
  }



  dismiss() {
    this.modal.dismiss({
      'dismissed': true
    });
  }


  private packEquipamento(): Equipment {
    var equipment: Equipment;
    equipment = new Equipment();

    equipment.setId(this.id);
    equipment.setName(this.name);
    equipment.setDateOfAquisition(this.data);
    //this.criaImagem();
   // equipment.setImage(this.selectImage());
    equipment.setImage(this.imageURI);

    return equipment;
  }

  pickImage(sourceType) : any {
    const options: CameraOptions = {
      quality: 40,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      return base64Image;
    }, (err) => {
      // Handle error
    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Selecione a imagem",
      buttons: [
      {
        text: 'Camera',
        handler: () => {
          this.imageURI = this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Voltar',
        role: 'Voltar'
      }
      ]
    });
    await actionSheet.present();

  }



}
