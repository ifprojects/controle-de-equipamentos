import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeEquipamentoPage } from './change-equipamento.page';

describe('ChangeEquipamentoPage', () => {
  let component: ChangeEquipamentoPage;
  let fixture: ComponentFixture<ChangeEquipamentoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeEquipamentoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeEquipamentoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
