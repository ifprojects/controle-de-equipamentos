import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChangeEquipamentoPage } from './change-equipamento.page';

const routes: Routes = [
  {
    path: '',
    component: ChangeEquipamentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChangeEquipamentoPage],
  exports : [ChangeEquipamentoPage]
})
export class ChangeEquipamentoPageModule {}
