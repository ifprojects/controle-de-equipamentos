import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProdutoServiceService } from 'src/app/services/ProdutoService/produto-service.service';
import { Equipment } from 'src/app/models/Equipamento';

@Component({
  selector: 'app-change-equipamento',
  templateUrl: './change-equipamento.page.html',
  styleUrls: ['./change-equipamento.page.scss'],
})
export class ChangeEquipamentoPage implements OnInit {

  itemId;
  itemName;
  public locationStatus: string;
  
  private serviceProduto : ProdutoServiceService;
  
  
  constructor(public modal: ModalController) { 
    this.serviceProduto = new ProdutoServiceService();
  }

  ngOnInit() {
    console.log(`${this.itemName}`)
  }

  update(){
    
    this.serviceProduto.updateProduct(this.packEquipamento(this.itemId));
  }

  dismiss(){
    this.modal.dismiss({
      'dismissed' : true
    });
  }

  private packEquipamento(itemId): Equipment{
    var equipment : Equipment;
    equipment = new Equipment();
    var aux = JSON.parse(localStorage.getItem("product"+this.itemId));
    console.log(aux);
    equipment.setName(this.itemName);
    equipment.setId(aux['id']);
    equipment.setDateOfAquisition(aux['dateOfAquisition']);
    equipment.setLocationStatus(this.locationStatus);
    return equipment;
  }


}
