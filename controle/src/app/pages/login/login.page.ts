import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from 'src/app/services/LoginService/login-service.service';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private login_service : LoginServiceService;

  enrollID : string;
  password : string;

  constructor(public routes : Router) { 
    this.login_service = new LoginServiceService();
  }


  login(){
    let usuario : Usuario;
    usuario = new Usuario();
    usuario.enrollID = this.enrollID;
    usuario.password = this.password;
    if(this.login_service.login(usuario)){
      this.routes.navigateByUrl('/home');
    }
  }

  logout(){
    this.login_service.logout();
  }

  ngOnInit() {


  }

}
