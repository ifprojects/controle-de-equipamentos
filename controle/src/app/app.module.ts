import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import {AddEquipamentoPageModule} from './pages/modals/add-equipamento/add-equipamento.module'
import {ChangeEquipamentoPageModule} from './pages/modals/change-equipamento/change-equipamento.module'

import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    AddEquipamentoPageModule, 
    ChangeEquipamentoPageModule],
  providers: [
    StatusBar,
    File,
    SplashScreen,
    Camera,
    File,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
