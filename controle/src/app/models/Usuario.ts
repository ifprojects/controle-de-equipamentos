export class Usuario{

/* 
    Classe abstarta Usuario

*/
    private _name: string;
    private _enrollID: string; 
    private _password: string;
    
    public constructor(){
    
    }

    
    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    
    public get enrollID(): string {
        return this._enrollID;
    }
    public set enrollID(value: string) {
        this._enrollID = value;
    }
    
    public get password(): string {
        return this._password;
    }
    public set password(value: string) {
        this._password = value;
    }
    
    
}