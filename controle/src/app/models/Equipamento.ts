import { Usuario } from './Usuario';
import { Base64 } from '@ionic-native/base64/ngx';

export class Equipment{

    private name : string;
    private id : string;
    private dateOfAquisition : Date;
    private locationStatus: string;
    private locator : Usuario;
    private image : string;

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getId(): string {
        return this.id;
    }

    public setId(id: string): void {
        this.id = id;
    }

    public getDateOfAquisition(): Date {
        return this.dateOfAquisition;
    }

    public setDateOfAquisition(dateOfAquisition: Date): void {
        this.dateOfAquisition = dateOfAquisition;
    }

    public isLocationStatus(): string {
        return this.locationStatus;
    }

    public setLocationStatus(locationStatus: string): void {
        this.locationStatus = locationStatus;
    }

    public getLocationStatus() : string {
        return this.locationStatus;
    }

    public getLocator(): Usuario {
        return this.locator;
    }

    public setLocator(locator: Usuario): void {
        this.locator = locator;
    }

    public setImage(image : string){
        this.image = image;
    }

    public getImage(){
        return this.image;
    }


}